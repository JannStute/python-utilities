#!/usr/bin/env python
import cv2
import numpy as np
import argparse
from pathlib import Path

def video(s: str):
    p = Path(s)
    if not p.is_file():
        raise argparse.ArgumentTypeError(f"\"{s}\" does not exist or is not a file.")
    v = cv2.VideoCapture(str(p))
    v.setExceptionMode(enable=0)
    if not v.get(cv2.CAP_PROP_FRAME_COUNT) > 0:
        raise argparse.ArgumentTypeError(f"\"{s}\" is not a video or has 0 frames")
    return v

def out(s: str):
    p = Path(s).absolute().resolve()
    if p.is_dir():
        raise argparse.ArgumentTypeError(f"\"{s}\" is a directory")
    if not p.parent.exists():
        raise argparse.ArgumentTypeError(f"\"{str(p.parent)}\" does not exist.")
    return str(p)

parser = argparse.ArgumentParser(description="Takes the average of all frames of a video and saves that average as an image.")
parser.add_argument("video", metavar="VIDEO", type=video, help="Path to the video file.")
parser.add_argument("imagePath", metavar="OUT", type=out, nargs="?", default="avg.jpg", help="Output path including the filename and filetype-extension (parent directory should exist). Default is \"avg.jpg\".")
args = parser.parse_args()

vid = args.video
sum = np.array([],np.dtype("uint32"))
curFrame = 0

while True:
    ret, frame = vid.read()
    if ret:
        if curFrame == 0:
            sum = np.array(frame,np.dtype("uint32"))
            sum = np.add(sum, frame)
        else:
            sum = np.add(sum, frame)
        curFrame += 1
        print(curFrame)
    else:
        vid.release()
        break

curFrame += 1
cv2.imwrite(args.imagePath, sum/curFrame)
print(f"saved to \"{args.imagePath}\"")
cv2.destroyAllWindows()
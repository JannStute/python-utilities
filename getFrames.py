#!/usr/bin/env python
import cv2
import os.path as pth
from pathlib import Path
import argparse

def video(s: str):
    p = Path(s)
    if not p.is_file():
        raise argparse.ArgumentTypeError(f"\"{s}\" does not exist or is not a file.")
    v = cv2.VideoCapture(str(p))
    v.setExceptionMode(enable=0)
    if not v.get(cv2.CAP_PROP_FRAME_COUNT) > 0:
        raise argparse.ArgumentTypeError(f"\"{s}\" is not a video or has 0 frames")
    return v

def out(s: str):
    p = Path(s).absolute().resolve()
    if not p.is_dir():
        raise argparse.ArgumentTypeError(f"\"{s}\" does not exist or is not a directory")
    return str(p)

parser = argparse.ArgumentParser(description="Saves the frames of a video as images.")
parser.add_argument("video", metavar="VIDEO", type=video, help="Path to the video file.")
parser.add_argument("outputDirectory", metavar="OUT", type=out, nargs="?", default=Path("data").absolute(), help="Path to the output directory. Default is \".\\data\".")
args = parser.parse_args()

vid = args.video

curFrame = 0

while True:
    ret, frame = vid.read()
    if ret:
        filename = pth.join(args.outputDirectory, str(curFrame)+".jpg")
        print(f"Saving Frame {curFrame}")

        cv2.imwrite(filename, frame)

        curFrame += 1
    else:
        break

vid.release()
print("done")